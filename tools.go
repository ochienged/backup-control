package main

import (
	"fmt"
	"os"
	"strings"
)

func getNamespace() string {
	if os.Getenv("NAMESPACE") == "" {
		panic(fmt.Errorf("Env NAMESPACE not set"))
	}

	return os.Getenv("NAMESPACE")
}

func getJobName() string {
	return os.Getenv("JOB_NAME")
}

func remoteCommand() string {
	if os.Getenv("REMOTE_COMMAND") != "" {
		return os.Getenv("REMOTE_COMMAND")
	}

	return "backup-utility"
}

// TaskRunner returns deployment name for task runner
func TaskRunner() string {
	if os.Getenv("GITLAB_NAME") == "" {
		panic(fmt.Errorf("Env GITLAB_NAME not set"))
	}
	return strings.Join([]string{os.Getenv("GITLAB_NAME"), "task-runner"}, "-")
}
