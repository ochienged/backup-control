package main

import (
	"fmt"
	"log"
	"reflect"
	"strings"
	"time"
)

func main() {
	log.Println("Initiating task...")
	startTime := time.Now()

	kClient, err := NewBackupClient()
	if err != nil {
		_ = kClient.UpdateJobStatus(startTime, time.Now(), CommandResult{}, fmt.Errorf("Error getting k8s client: %v", err))
		panic(err)
	}

	tr, err := kClient.TaskRunnerDeployment()
	if err != nil {
		_ = kClient.UpdateJobStatus(startTime, time.Now(), CommandResult{}, fmt.Errorf("Task Runner deployment not found: %v", err))
		panic(err)
	}

	pods, err := kClient.DeploymentPods(tr)
	if err != nil {
		_ = kClient.UpdateJobStatus(startTime, time.Now(), CommandResult{}, fmt.Errorf("Deployment pods not found: %v", err))
		panic(err)
	}

	command := strings.Split(remoteCommand(), " ")
	result, err := kClient.ExecuteCommandInPod(TaskRunnerBackupPod(pods), "task-runner", getNamespace(), command)
	if err != nil {
		_ = kClient.UpdateJobStatus(startTime, time.Now(), CommandResult{}, fmt.Errorf("Job failed: %v", err))
		log.Printf(err.Error())
	}

	if !reflect.DeepEqual(result, CommandResult{}) && getJobName() != "" {
		if err := kClient.UpdateJobStatus(startTime, time.Now(), result, nil); err != nil {
			log.Printf(err.Error())
		}
	}

	logCommand(result)
}

func logCommand(result CommandResult) {
	if !reflect.DeepEqual(result, CommandResult{}) {
		if result.Output() != "" {
			log.Printf("Output:\n%s\n", result.Output())
		}

		if result.Error() != "" {
			log.Printf("Error:\n%s\n", result.Error())
		}
	}
}
