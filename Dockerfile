FROM golang:alpine AS builder

RUN apk update \
    && apk add --no-cache git
WORKDIR $GOPATH/src/backup-control
COPY . .
RUN go get -d -v \
    && go build -o /go/bin/backup-control

FROM alpine

COPY --from=builder /go/bin/backup-control /usr/local/bin/backup-control

ENTRYPOINT ["/usr/local/bin/backup-control"]
