package main

import (
	"bytes"
	"os"
	"path/filepath"
	"reflect"
	"time"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/tools/remotecommand"
)

// BackupClient defines client used to
// interact with kubernetes
type BackupClient struct {
	// client-go client
	client *kubernetes.Clientset
	// kubernetes config
	config *rest.Config
}

// CommandResult returns
// command results
type CommandResult struct {
	stdOut string
	stdErr string
}

// Output returns standard output
func (c CommandResult) Output() string {
	return c.stdOut
}

// Error returns standard error
func (c CommandResult) Error() string {
	return c.stdErr
}

// NewBackupClient creates a new backup client that
// can be used to interact with kubernetes API
func NewBackupClient() (BackupClient, error) {
	config, err := rest.InClusterConfig()
	if err != nil {
		config, err = clientcmd.BuildConfigFromFlags("",
			filepath.Join(os.Getenv("HOME"), ".kube", "config"))
		if err != nil {
			return BackupClient{}, err
		}
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return BackupClient{}, err
	}

	return BackupClient{
		config: config,
		client: clientset,
	}, nil
}

// TaskRunnerDeployment defines the name of the
func (b BackupClient) TaskRunnerDeployment() (*appsv1.Deployment, error) {
	target, err := b.client.AppsV1().Deployments(getNamespace()).Get(TaskRunner(), metav1.GetOptions{})
	if err != nil {
		return &appsv1.Deployment{}, err
	}

	return target, nil
}

// DeploymentPods returns list of pods associated with a given deployment
func (b BackupClient) DeploymentPods(obj *appsv1.Deployment) (*corev1.PodList, error) {

	podLabels := labels.Set(obj.Spec.Template.Labels)

	pods, err := b.client.CoreV1().Pods(getNamespace()).List(
		metav1.ListOptions{LabelSelector: podLabels.AsSelector().String()},
	)
	if err != nil {
		return &corev1.PodList{}, err
	}

	return pods, nil
}

// ExecuteCommandInPod runs command in pod
// it returns the standard out and errors as string
func (b BackupClient) ExecuteCommandInPod(podName, container, namespace string, command []string) (CommandResult, error) {

	options := &corev1.PodExecOptions{
		Container: container,
		Command:   command,
		Stdin:     false,
		Stderr:    true,
		Stdout:    true,
		TTY:       false,
	}

	req := b.client.CoreV1().RESTClient().Post().
		Resource("pods").
		Name(podName).
		Namespace(namespace).
		SubResource("exec").VersionedParams(options, scheme.ParameterCodec)

	exec, err := remotecommand.NewSPDYExecutor(b.config, "POST", req.URL())
	if err != nil {
		return CommandResult{}, err
	}

	var stdout, stderr bytes.Buffer
	err = exec.Stream(remotecommand.StreamOptions{
		Stdin:  nil,
		Stdout: &stdout,
		Stderr: &stderr,
	})
	if err != nil {
		return CommandResult{}, err
	}

	return CommandResult{
		stdOut: stdout.String(),
		stdErr: stderr.String(),
	}, nil
}

// TaskRunnerBackupPod returns single pod on which to run command
func TaskRunnerBackupPod(list *corev1.PodList) string {

	for _, pod := range list.Items {
		if pod.Status.Phase == corev1.PodRunning {
			return pod.Name
		}
	}

	return ""
}

// UpdateJobStatus writes job results to configmap
func (b BackupClient) UpdateJobStatus(start, stop time.Time, result CommandResult, err error) error {
	lockCM, err := b.client.CoreV1().ConfigMaps(getNamespace()).Get(getJobName(), metav1.GetOptions{})
	if err != nil {
		return err
	}

	lockCM.Data = map[string]string{
		"startTime": start.Format(time.RFC3339),
		"stopTime":  stop.Format(time.RFC3339),
	}

	if !reflect.DeepEqual(result, CommandResult{}) {
		lockCM.Data["output"] = result.Output()
		lockCM.Data["error"] = result.Error()
	}

	if err != nil {
		lockCM.Data["error"] = err.Error()
	}

	_, err = b.client.CoreV1().ConfigMaps(getNamespace()).Update(lockCM)
	return err
}
